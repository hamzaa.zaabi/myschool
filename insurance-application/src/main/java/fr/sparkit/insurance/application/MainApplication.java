package fr.sparkit.insurance.application;


import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

import javax.annotation.PostConstruct;

@EnableAuthorizationServer
@Configuration
@SpringBootApplication(scanBasePackages = "fr.sparkit.insurance")
@EntityScan("fr.sparkit.insurance.entities")
@ComponentScan("fr.sparkit.insurance")
@EnableJpaRepositories("fr.sparkit.insurance")
@EnableAsync
@Slf4j
public class MainApplication extends SpringBootServletInitializer {
    private static final Logger LOG = LoggerFactory.getLogger(MainApplication.class);


    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }


}
