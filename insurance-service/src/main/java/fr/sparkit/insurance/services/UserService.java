package fr.sparkit.insurance.services;

import fr.sparkit.insurance.dto.UserDto;
import fr.sparkit.insurance.entities.Users;
import fr.sparkit.insurance.utils.http.CustomNotificationException;
import org.springframework.data.domain.Page;

public interface UserService {
    UserDto addUser(UserDto user ) ;
    Page<Users> getAllUsers(int size ,int page);
    UserDto getUser(Long id);
    boolean deleteUser(Long id);
    UserDto updateUser(Long id , UserDto user);

}
