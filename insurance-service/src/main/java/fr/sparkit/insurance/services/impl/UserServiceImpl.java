package fr.sparkit.insurance.services.impl;

import fr.sparkit.insurance.converter.UserConverter;
import fr.sparkit.insurance.dto.UserDto;
import fr.sparkit.insurance.services.UserService;
import fr.sparkit.insurance.dao.IUserDao;
import fr.sparkit.insurance.entities.Users;
import fr.sparkit.insurance.utils.errors.ApiErrors;
import fr.sparkit.insurance.utils.http.CustomNotificationException;
import fr.sparkit.insurance.utils.http.EntitiesException;
import io.swagger.annotations.Api;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;


@Service
public class UserServiceImpl implements UserService {
    private IUserDao iUserDao;

    @Autowired
    public UserServiceImpl(IUserDao iUserDao) {
        this.iUserDao = iUserDao;
    }

    @Override
    public UserDto addUser(UserDto user)  {
        boolean isUserRequiredAttributeNull = user.getEmail() == null || user.getPassword() == null || user.getPhone() == null;
        if(isUserRequiredAttributeNull){
            throw  new EntitiesException(474474);
        }
        boolean isEmailAllReadyUsed = this.iUserDao.findByEmail(user.getEmail()).isPresent();
        boolean isPhoneAllReadyUsed =this.iUserDao.findByPhone(user.getPhone()).isPresent();
        if(isEmailAllReadyUsed){
               throw  new CustomNotificationException(ApiErrors.insurance.USER_DUPLICATE_EMAIL);
        } else if(isPhoneAllReadyUsed){
               throw  new CustomNotificationException(ApiErrors.insurance.USER_DUPLICATE_PHONE);
           }

        Users userToSave = UserConverter.dtoToModel(user);
        Users userSaved = iUserDao.save(userToSave);
        return UserConverter.modeToDto(userSaved);

    }

    @Override
    public Page<Users> getAllUsers(int size,int page) {
        PageRequest pageRequest = PageRequest.of(page ,size);
        return this.iUserDao.findAll(pageRequest);
    }

    @Override
    public UserDto getUser(Long id) {
        Users user = this.iUserDao.findOne(id).orElseThrow(()-> new CustomNotificationException(ApiErrors.insurance.NULL_USER_FOUND));
        return UserConverter.modeToDto(user);
    }

    @Override
    public boolean deleteUser(Long id) {
        this.iUserDao.findOne(id).orElseThrow(()->
                                   new CustomNotificationException(ApiErrors.insurance.NULL_USER_FOUND));
        this.iUserDao.delete(id, UUID.randomUUID());
        return true;
    }

    @Override
    public UserDto updateUser(Long id, UserDto user) {
        this.iUserDao.findOne(id).orElseThrow(()->
                                  new CustomNotificationException(ApiErrors.insurance.NULL_USER_FOUND));
        user.setId(id);
        Users userToSave = UserConverter.dtoToModel(user);
        Users userSaved = this.iUserDao.save(userToSave);
        return UserConverter.modeToDto(userSaved);
    }


}
