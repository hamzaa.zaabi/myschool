package fr.sparkit.insurance.converter;

import fr.sparkit.insurance.dto.UserDto;
import fr.sparkit.insurance.entities.Users;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserConverter {
    public UserConverter(){

    }

    private static  final ModelMapper mapper = new ModelMapper();

    public static Users dtoToModel(UserDto userDto){
        return mapper.map(userDto ,Users.class);
    }

    public static UserDto modeToDto(Users user){
        return mapper.map(user,UserDto.class);
    }

    public static List<UserDto> modelsToDtoS(List<Users> users){
      return   users.stream().filter(Objects::nonNull).map(UserConverter::modeToDto).collect(Collectors.toList());
    }
}
