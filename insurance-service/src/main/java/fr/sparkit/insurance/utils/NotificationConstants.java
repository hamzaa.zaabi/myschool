package fr.sparkit.insurance.utils;

public final class NotificationConstants {
    /* COMMENTS SPACE */
    public static final String ON_ADD_COMMENT_CONTENT = "Un nouveau commentaire a été ajouté par ";
    public static final String NOTIF_COMMENT_CONTENT = "Commentaire";

    /* FOLDER */
    public static final String ON_ADD_FOLDER_CONTENT = "Un nouveau dossier a été créé par ";
    public static final String ON_FOLDER_VALIDATION_CONTENT = " : a été validé par ";
    public static final String ON_FOLDER = "Dossier Numéro ";
    public static final String NOTIF_FOLDER_CONTENT = "Dossier";


    /* MECHANIC */
    public static final String ON_MECHANICS_AFFECTION_CONTENT = " : affectation des garagistes par ";
    public static final String NOTIF_MECHANIC_CONTENT = "Garagiste";


    /* EXPERT */
    public static final String ON_EXPERT_AFFECTION_CONTENT = " : affectation d'un expert par ";
    public static final String NOTIF_EXPERT_CONTENT = "Expert";

    /* QUOTE */
    public static final String ON_INITIAL_QUOTE_ADD_CONTENT = " : Un devis initial a été ajouté par ";
    public static final String ON_ADDITIONAL_QUOTE_ADD_CONTENT = " : Un devis complémentaire a été ajouté par ";
    public static final String ON_END_REPAIR_QUOTE_ADD_CONTENT = " : Un devis de fin de réparation a été ajouté par ";

    public static final String ON_INITIAL_QUOTE_APPROVE_CONTENT = " : le devis initial a été approuvé par ";
    public static final String ON_ADDITIONAL_QUOTE_APPROVE_CONTENT = " : le devis complémentaire a été approuvé par ";
    public static final String ON_ADDITIONAL_QUOTE_DISAPPROVE_CONTENT = " : le devis complémentaire a été rejeté par ";
    public static final String ON_END_REPAIR_QUOTE_APPROVE_CONTENT = " : le devis de fin de réparation a été approuvé par ";

    public static final String ON_NEGOTIATION_QUOTE_ADD_CONTENT = " : Un devis de négociation a été ajouté par ";
    public static final String ON_QUOTE_SELECT = " : Un devis a été choisi par ";
    public static final String NOTIF_QUOTE_CONTENT = "Devis";
}
