package fr.sparkit.insurance.utils;

public class InsuranceConstants {
    public static final class AuditableFieldsConstants {
        public static final String LAST_MODIFIED_BY = "lastModifiedDate";

        private AuditableFieldsConstants() {

        }
    }
}
