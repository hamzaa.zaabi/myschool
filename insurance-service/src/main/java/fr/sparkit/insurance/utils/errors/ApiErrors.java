package fr.sparkit.insurance.utils.errors;

public final class ApiErrors {

    private ApiErrors() {
        super();
    }

    public static final class insurance {
        /* Naming convention : [ENTITY]_[SHORT_DESCRIPTION] */
        /* Generic insurance codes [40000-40099] */
        public static final int ENTITY_NOT_FOUND = 40000;

        /* User codes [40100-40199] */
        public static final int NULL_USER_FOUND = 40100;
        public static final int USER_DUPLICATE_EMAIL = 40101;
        public static final int USER_DUPLICATE_PHONE = 40102;
        public static final int USER_IS_AFFECTED_TO_FOLDER = 40103;
        public static final int NO_ADMIN_FOUND = 40104;
        public static final int NO_SUPER_ADMIN_FOUND = 40105;
        public static final int NO_INSURED_CLIENT_ADMIN_FOUND = 40106;
        /* Folder codes [40200-40299] */
        public static final int NULL_FOLDER_NOT_FOUND = 40200;
        public static final int NULL_USER_FOLDER_ATTACHMENT_FOUND = 40201;

        /* Camunda codes [40200-40299] */
        public static final int NULL_WKF_WITH_KEY_LIKE_FOUND = 40200;
        public static final int NULL_TASKS_BY_INSTANCE_ID_FOUND = 40201;

        /* Folder codes [40300-40399] */
        public static final int FOLDER_NOT_FOUND = 40300;
        public static final int FOLDER_IS_VALID = 40301;

        /* roles codes [40400-40499] */
        public static final int ROLE_NOT_FOUND = 40400;

        /* file codes [40500-40599] */
        public static final int FILES_NOT_FOUND = 40500;

        /* comment codes [40600-40699] */
        public static final int COMMENT_NOT_FOUND = 40600;

        /* comment codes [40700-40799] */
        public static final int NOTIFICATION_NOT_FOUND = 40700;

        /* insurance codes [40800-40899] */
        public static final int NULL_INSURANCE_FOUND = 40100;

        /* comment codes [40900-40999] */
        public static final int DELETE_FOLDER_IS_IMPOSSIBLE = 40900;

        private insurance() {
            super();
        }
    }

}
