package fr.sparkit.insurance.utils;

public final class PropertiesConstants {
    public static final String PREFIX = "host";
    public static final String TIMEZONE_COOKIE = "TIMEZONE_COOKIE";
    private PropertiesConstants() {

    }
}
