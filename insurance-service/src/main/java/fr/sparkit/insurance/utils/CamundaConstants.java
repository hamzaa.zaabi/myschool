package fr.sparkit.insurance.utils;

public final class CamundaConstants {
    public static final String SUBMIT_FORM_WORKFLOW = "/process-definition/";
    public static final String GET_TASK_ID_URL = "/task";
    public static final String MODEL_ID = "InsuranceWKF";
    public static final String SUBMIT = "/submit-form";
    public static final String COMPLETE = "/complete";
}
