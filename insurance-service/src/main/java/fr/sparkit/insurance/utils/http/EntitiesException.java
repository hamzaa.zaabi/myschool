package fr.sparkit.insurance.utils.http;

import fr.sparkit.insurance.utils.errors.ErrorsResponse;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class EntitiesException extends RuntimeException {
    private final int errorCode;
    private final ErrorsResponse errorsResponse;

    public EntitiesException(int errorCode) {
        super();
        this.errorCode = errorCode;
        this.errorsResponse = new ErrorsResponse();
    }
    public EntitiesException(int errorCode, ErrorsResponse errorsResponse) {
        this.errorCode = errorCode;
        this.errorsResponse = errorsResponse;
    }
}
