package fr.sparkit.insurance.services.impl;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class FolderServiceImplTest {

//
//    @Mock
//    private IUserService userService;
//    @Mock
//    private IAttachmentDao attachmentDao;
//    @Mock
//    private IFolderDao folderDao;
//    @Mock
//    private IUserDao userDao;
//    @Mock
//    private ICamundaService camundaService;
//    @Mock
//    private INotificationService notificationService;
//    @Mock
//    private IUserFolderAttachmentDao folderAttachmentDao;
//
//    @InjectMocks
//    private FolderServiceImpl folderService;
//
//
//    @BeforeEach
//    void setup() {
//        folderService = new FolderServiceImpl(folderDao, userDao, camundaService, notificationService,
//                attachmentDao, folderAttachmentDao, userService);
//
//    }
//
//
//    @Test
//    public void should_return_list_of_elements() {
//        when(userService.getConnectedUser()).thenReturn(FolderUtils.getUser());
//        when(this.userDao.findById(any())).thenReturn(Optional.of(FolderUtils.getUser()));
//        when(this.folderDao.findById(any())).thenReturn(Optional.of(FolderUtils.getFolder()));
//        Folder folder = FolderUtils.getFolder();
//        folder.setUserFolderAttachment(FolderUtils.userFolderAttachmentClient());
//        when(this.folderDao.saveAndFlush(any())).thenReturn(folder);
//        when(this.folderAttachmentDao.findByUserTypeAndByFolderId(any(), any())).thenReturn(FolderUtils.userFolderAttachment());
//        folderService.updateFolder(FolderUtils.folderDto(), 1L);
//    }
//
//    @Test
//    public void should_return_exception() {
//        when(this.folderDao.findById(any())).thenReturn(Optional.empty());
//        when(this.folderAttachmentDao.findByUserTypeAndByFolderId(any(), any())).thenReturn(FolderUtils.userFolderAttachment());
//        FolderDto dto = new FolderDto();
//        dto.setExpertId(2L);
//        assertThrows(Exception.class, () -> {
//            folderService.updateFolder(dto, 1L);
//        });
//    }
//
//    @Test
//    public void should_return_exception_if_file_not_present() {
//        when(this.folderDao.findById(any())).thenReturn(Optional.empty());
//        CustomNotificationException exception = assertThrows(CustomNotificationException.class, () -> {
//            this.folderService.mechanicsAddFiles(1L);
//        });
//        assertThat(exception.getErrorCode()).isEqualTo(ApiErrors.insurance.FOLDER_NOT_FOUND);
//    }
//
//    @Test
//    public void should_return_set_of_long() {
//        Folder folder = FolderUtils.getFolder();
//
//        when(this.folderDao.findById(any())).thenReturn(Optional.of(folder));
//
//        Set<Long> result = this.folderService.mechanicsAddFiles(1L);
//
//        assertThat(result.size()).isEqualTo(1);
//        assertThat(result.contains(1L)).isEqualTo(true);
//    }
//
//    @Test
//    public void should_return_set_of_attachments() {
//        String absolutePath = Paths.get("").toAbsolutePath().toString();
//        ReflectionTestUtils.setField(folderService, "path", absolutePath.concat("\\src\\test\\java\\FILES\\public\\assets\\Folders\\"));
//        Set<AttachmentDto> result = this.folderService.getAttachmentByUserRole(5L);
//
//        assertThat(result.size()).isEqualTo(0);
//    }

}