package fr.sparkit.insurance.services.impl;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class DevisServiceImplTest {
//    @Mock
//    private IFolderDao folderDao;
//    @InjectMocks
//    private DevisServiceImpl devisService;
//    @Mock
//    private IUserDao userDao;
//    @Mock
//    private ICamundaService camundaService;
//    @Mock
//    private IUserFolderAttachmentDao iUserFolderAttachmentDao;
//    @Mock
//    private IFolderService iFolderService;
//    @Mock
//    private IUserService userService;
//
//    @BeforeEach
//    void setup() {
//        devisService = new DevisServiceImpl(folderDao, userDao, camundaService,
//                iUserFolderAttachmentDao, iFolderService, userService);
//
//    }
//
//    @Test
//    public void should_return_exception_if_Devis_not_present() {
//        when(this.folderDao.findById(any())).thenReturn(Optional.empty());
//        CustomNotificationException exception = assertThrows(CustomNotificationException.class, () -> {
//            this.devisService.selectDevis(1L, 1L);
//        });
//        assertThat(exception.getErrorCode()).isEqualTo(ApiErrors.insurance.FOLDER_NOT_FOUND);
//    }
//
//    @Test
//    public void should_validate_devis() {
//
//        Folder folder = FolderUtils.getFolder();
//        when(this.folderDao.findById(any())).thenReturn(Optional.of(folder));
//
//
//        when(this.iFolderService.getMechanicFolder(anyList())).thenReturn(FolderUtils.userFolderAttachment());
//        when(this.iUserFolderAttachmentDao.findById(any())).thenReturn(Optional.of(FolderUtils.getUserFolderAttachment()));
//        when(this.iUserFolderAttachmentDao.save(any())).thenReturn(FolderUtils.getUserFolderAttachment());
//        // when(this.iFolderService.notifyUsersOnSelectQuote(any()))
//
//        this.devisService.selectDevis(1L, 1L);
//        verify(this.iFolderService, times(1)).notifyUsersOnSelectQuote(any());
//
//    }

}