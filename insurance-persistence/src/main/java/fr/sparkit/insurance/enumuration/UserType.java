package fr.sparkit.insurance.enumuration;

public enum UserType {
    SUPER_ADMIN,
    ADMIN,
    CLIENT,
    INSURANCE,
    EXPERT,
    MECHANIC
}
