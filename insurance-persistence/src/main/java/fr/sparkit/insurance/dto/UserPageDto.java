package fr.sparkit.insurance.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class UserPageDto {
    private List<UserDto> userDtoList;
    private Long totalElements;
}
