package fr.sparkit.insurance.dao;

import fr.sparkit.insurance.entities.Users;
import fr.sparkit.insurance.enumuration.UserType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface IUserDao extends BaseRepository<Users , Long>  {

  Optional<Users> findByEmail(String email);
  Optional<Users> findByPhone(String phone);

  @Override
  Optional<Users> findOne(Specification spec);

  @Override
  Page<Users> findAll(Pageable pageable);
}
