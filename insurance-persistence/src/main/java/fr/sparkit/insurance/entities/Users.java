package fr.sparkit.insurance.entities;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "USERS")
public class Users  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USE_ID")
    @EqualsAndHashCode.Include
    private Long id;
    @Column(name = "USE_FIRSTNAME")
    private String firstName;
    @Column(name = "USE_LASTNAME")
    private String lastName;
    @Column(name = "USE_ADDRESS")
    private String address;
    @Column(name = "USE_Email", unique = true, nullable = false)
    private String email;
    @Column(name = "USR_PASSWORD", nullable = false)
    private String password;
    @Column(name = "USE_PHONE" , unique = true, nullable = false)
    private String phone;
    @Lob
    @Column(name = "USE_PHOTO")
    private String photo;
    @Column(name = "USE_IS_DELETED", columnDefinition = "bit default 0")
    private boolean isDeleted;
    @Column(name = "USE_DELETED_TOKEN")
    private UUID deletedToken;




}
