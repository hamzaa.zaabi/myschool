package fr.sparkit.insurance.restcontroller.utils;

import fr.sparkit.insurance.utils.http.CustomNotificationException;
import fr.sparkit.insurance.utils.http.EntitiesException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice()
@Slf4j
public class RestControlAdviser extends ResponseEntityExceptionHandler {
    public static final int CUSTOM_STATUS_ERROR_CODE_NOTIFICATION = 224;

    @ExceptionHandler({ CustomNotificationException.class })
    public CustomeErrorResponse handleCustomerNotficiationException(CustomNotificationException exception,
                                                                    HttpServletResponse response) {
        CustomeErrorResponse httpErrorResponse = new CustomeErrorResponse(exception.getErrorCode(),
                exception.getErrorsResponse().getErrors(), exception.getMessage());
        response.setStatus(CUSTOM_STATUS_ERROR_CODE_NOTIFICATION);

        log.error("Custom Exception Notification => error code : {} , details : {}", exception.getErrorCode(),
                exception.getErrorsResponse().getErrors());
        return httpErrorResponse;
    }
    @ExceptionHandler({ EntitiesException.class })
    public CustomeErrorResponse handleException(EntitiesException exception,
                                                                    HttpServletResponse response) {
        CustomeErrorResponse httpErrorResponse = new CustomeErrorResponse(exception.getErrorCode(),
                                      exception.getErrorsResponse().getErrors() ,exception.getMessage());
        response.setStatus(CUSTOM_STATUS_ERROR_CODE_NOTIFICATION);

        return httpErrorResponse;
    }

}
