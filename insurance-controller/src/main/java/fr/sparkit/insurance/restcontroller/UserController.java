package fr.sparkit.insurance.restcontroller;

import fr.sparkit.insurance.dto.UserDto;
import fr.sparkit.insurance.services.UserService;
import fr.sparkit.insurance.entities.Users;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/insurance/User")
@Slf4j
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping("/add-user")
    public UserDto addUser(@RequestBody UserDto user) {
        return userService.addUser(user);
    }

    @GetMapping("/get-all-users")
    public Page<Users> getAllUsers(@PathParam("page") int page , @PathParam("size") int size) {
                return this.userService.getAllUsers( page ,size) ;
    }

    @GetMapping("/{id}")
    public  UserDto getUserById(@PathVariable Long id){
        return this.userService.getUser(id);
    }

    @DeleteMapping("/delete-user/{id}")
    public boolean deleteUser(@PathVariable Long id){
      return   this.userService.deleteUser(id);
    }

    @PutMapping("/update-user/{id}")
    public UserDto updateUser(@RequestBody UserDto user , @PathVariable Long id){
      return   this.userService.updateUser(id,user);
    }

}

